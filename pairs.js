const pairs = (obj) => {
  if (typeof obj !== "object" || obj === null) {
    throw new Error("Input must be an object");
  }

  const result = [];

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      const pair = [key, obj[key]];
      result.push(pair);
    }
  }

  return result;
}
module.exports = pairs;
