const values = (obj) => {
    if (typeof obj !== 'object' || obj === null) {
        throw new Error('Input must be an object');
    }

    const result = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key) && typeof obj[key] !== 'function') {
            result.push(obj[key]);
        }
    }
    return result;
}
module.exports = values;