const invert = require("../invert.js");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

try {
  console.log(invert(testObject));
} catch (error) {
  console.error(error.message);
}
