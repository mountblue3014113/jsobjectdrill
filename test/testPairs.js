const pairs = require("../pairs.js");
const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

try {
  console.log(pairs(testObject));
} catch (error) {
  console.error(error.message);
}
