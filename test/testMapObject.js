const mapObject = require("../mapObject.js");
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

try {
    const mappedObject = mapObject(testObject, (val, key) => val+ '-Transformed');
console.log(mappedObject); 
} catch (error) {
    console.error(error.message);
}