const invert = (obj) => {
  if (typeof obj !== "object" || obj === null) {
    throw new Error("Input must be an object");
  }

  const invertedObj = {};

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      invertedObj[obj[key]] = key;
    }
  }

  return invertedObj;
};
module.exports = invert;
