const keys = (obj) => {
    if (typeof obj !== 'object' || obj === null) {
        throw new TypeError('Object expected');
    }
    
    const result = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            result.push(key);
        }
    }
    return result;
}

module.exports = keys;
