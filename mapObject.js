const mapObject = (obj, cb) => {
  if (typeof obj !== "object" || typeof cb !== "function") {
    throw new Error("obj must be an object and cb must be a function");
  }

  const mappedObject = {};

  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      mappedObject[key] = cb(obj[key], key);
    }
  }

  return mappedObject;
};
module.exports = mapObject;
