const defaults = (obj, byDefault) => {
  if ( typeof obj !== "object" || obj === null || typeof byDefault !== "object" || byDefault === null) {
    throw new Error("Inputs must be objects");
  }

  for (const key in byDefault) {
    if (byDefault.hasOwnProperty(key) && obj[key] === undefined) {
      obj[key] = byDefault[key];
    }
  }

  return obj;
}

module.exports = defaults;
